<?php


class Config
{
    const HOST = 'localhost';
    const USER = 'root';
    const PASS = 'girnar';
    const DB = 'php';
    const SITE_NAME = 'Practice';

    const WWW = __DIR__;
    const BASE = __DIR__;
    const HOME = __DIR__;
    const ROOT = __DIR__;
    const CORE = __DIR__ . '/Core';
    const LIB = __DIR__.'/lib';

    const SMS_API = 'localhost';
    const SMS_TOKEN = 'php';

    const EMAIL_USER = 'root';
    const EMAIL_PASSWORD = 'girnar';

    const PHONE = '8527672265';
    const PUSH_NOTIFICATION = true;
    const LOCATION_ENABLE = true;
    const GA = true;

    const MINIFY_PATH = __DIR__.'/Public/minify';
    const MINIFY_JS_PATH = self::MINIFY_PATH.'/js';
    const MINIFY_CSS_PATH = self::MINIFY_PATH.'/css';
    const MINIFY_IMAGES_PATH =  self::MINIFY_PATH.'/images';

    const JS_PATH =  __DIR__.'/Public/js';
    const CSS_PATH =  __DIR__.'/Public/css';
    const IMAGES_PATH =  __DIR__.'/Public/images';

    public final function getConst(){

    }
}