<?php

function my_autoloader($class) {
    require_once('Core/' . $class . '.php');
}

spl_autoload_register('my_autoloader');

list($db, $config) = Connection::getCon();

require_once __DIR__ . '/Vendor/autoload.php';
use MatthiasMullie\Minify;

$sourcePath = Config::CSS_PATH.'/bootstrap.css';
$minifier = new Minify\CSS($sourcePath);
// or we can just add plain CSS
$css = 'body { color: #000000; }';
$minifier->add($css);
// save minified file to disk
$minifiedPath = Config::MINIFY_CSS_PATH.'/minify.css';
$minifier->minify($minifiedPath);

