<?php
require_once('Config.php');

class Connection extends Config {
    private static $obj;
    private static $db;

    private final function __construct()
    {
         //
    }

    public final function pr($array)
    {
        echo '<pre>';
            print_r($array);
        echo '</pre>';
    }

    public static function getCon(){
        if(!isset(self::$obj)){
            self::$obj = new Connection();
            self::$db = mysqli_connect(self::HOST, self::USER, self::PASS, self::DB);
        }
        return array(self::$db, self::$obj);
    }
}


